# Dotfiles Setup Script

This is a setup script for my dotfiles on a macOS or Debian/Ubuntu Linux system.  
The script installs and configures various applications, plugins, and font.

## Prerequisites

Before running the script, ensure that the following are installed:

| Apps to install |
| --------------- |
| git             |
| curl            |
| sudo            |

## Installation

Clone the repository:

```bash
git clone https://github.com/username/dotfiles.git ~/.dotfiles
```

Navigate to the cloned repository:

```bash
cd ~/.dotfiles
```

Run the script:

```bash
python3 install.py
```

Follow the prompts to install and configure the various applications.

## Features

The script performs the following actions:

- Installs and configures Homebrew and packages specified in a Brewfile
- Installs zsh and Oh My Zsh with plugins
- Links various dotfiles to their appropriate locations
- Installs and configures fonts
- Installs asdf and sets up the latest versions of nodejs, awscli, and golang
- Installs and sets up plugins for nvim
- Sources tmux configuration

## Notes

The script is intended for use on a macOS or Debian/Ubuntu Linux system.

Before running the script, ensure that the git and curl commands are available in the terminal.  
Some of the installation prompts can be skipped by answering "n" when prompted.  
Some of the configurations may overwrite existing configurations.
The script can be run multiple times without issue.  
The script can be modified to install additional applications or configurations as needed.

Authors:  
Ben
