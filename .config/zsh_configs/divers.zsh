# echo "divers loaded"

# To activate prompt kubernetes visualisation
#source "/opt/homebrew/opt/kube-ps1/share/kube-ps1.sh"
#PS1='$(kube_ps1)'$PS1
#

# Kubernetes package manager
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

# Dailymotion config for ssh with Yubikey
# export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
# killall gpg-agent || true
# eval $(gpg-agent --daemon)

# Color theme for Exa
#export LS_COLORS="$(vivid generate snazzy)"

# ASDF
. "$HOME/.asdf/asdf.sh"

# add color to man pages
export MANROFFOPT='-c'
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2)
export LESS_TERMCAP_md=$(tput bold; tput setaf 6)
export LESS_TERMCAP_me=$(tput sgr0)
export LESS_TERMCAP_so=$(tput bold; tput setaf 3; tput setab 4)
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 7)
export LESS_TERMCAP_ue=$(tput rmul; tput sgr0)
export LESS_TERMCAP_mr=$(tput rev)
export LESS_TERMCAP_mh=$(tput dim)
