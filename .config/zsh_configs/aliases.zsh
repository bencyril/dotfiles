echo "alias loaded"

# Alias
# ---
#
alias k="kubectl"

# mac OS shortcuts
alias code="open -a 'Visual Studio Code'"

# ALIAS COMMANDS
alias ls="exa --icons --group-directories-first"
alias ll="exa --icons --group-directories-first -l"
alias g="goto"
alias grep='grep --color'
alias pcp="rsync -r --progress"
alias mosh="mosh --no-init"
alias fd="fd -Hi"

# alias ls="exa"
alias history="history -E"
alias k="kubectl"
alias dip='docker inspect --format {{ .NetworkSettings.IPAddress }} '
alias ipp='dig +short myip.opendns.com @resolver1.opendns.com'
alias bigfile='du -sch .[!.]* * |sort -h | egrep "M|G"'

# GIT
alias ggpull='git pull origin "$(git_current_branch)"'
alias ggpush='git push origin "$(git_current_branch)"'
alias gst='git status'

# KUBERNETES
alias k='kubectl'
alias kgp='kubectl get pod'
alias kgs='kubectl get service'
