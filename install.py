import os
import subprocess
import platform
import sys
import shutil
from urllib.request import urlretrieve

def run_command(command):
    try:
        subprocess.run(command, shell=True, check=True)
    except subprocess.CalledProcessError as e:
        check_err(e)

def check_err(error):
    print(f"Error: {error}")
    sys.exit(1)

def check_if_app_exists(app_name):
    return shutil.which(app_name) is not None

def install_app(app):
    app_list = ["curl", "git", "go", "node"]

    if check_if_app_exists(app):
        print(f"{app} is already installed.")
        return

    if app in app_list:
        print(f"Installing {app}...")
    elif input(f"Do you want to install {app}? (y/n): ").lower() != "y":
        print(f"{app} installation skipped.")
        return

    os_type = platform.system()

    if os_type == "Darwin":
        print(f"Installing {app} with Homebrew...")
        run_command(f"brew install {app}")
    elif os_type == "Linux":
        distro = platform.freedesktop_os_release()['NAME'].lower()

        if "ubuntu" in distro or "debian" in distro:
            print(f"Installing {app} with apt...")
            run_command(f"sudo apt update && sudo apt install -y {app}")
        else:
            print("Unsupported Linux distribution. Please install {app} manually.")
    else:
        print("Unsupported platform. Please install {app} manually.")

def install_oh_my_zsh():
    if not check_if_app_exists("zsh"):
        install_app("zsh")
    else:
        print("Zsh installation skipped.")
        return

    if not os.path.exists(os.path.expanduser("~/.oh-my-zsh")):
        if input("Do you want to install Oh My Zsh? (y/n): ").lower() == "y":
            install_app("curl")
            install_app("git")

            run_command('sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"')
            print("Oh My Zsh installed successfully!")
        else:
            print("Oh My Zsh installation skipped.")
    else:
        print("Oh My Zsh is already installed.")

def create_config_directory():
    config_dir = os.path.expanduser("~/.config")
    if not os.path.exists(config_dir):
        os.makedirs(config_dir)

def link_dotfiles():
    source_dir = os.path.expanduser("~/.dotfiles/.config")
    target_dir = os.path.expanduser("~/.config")

    folders_to_link = ["nvim", "kitty", "zsh_configs"]
    files_to_link = [".tmux.conf", ".tmux.conf.local"]
    
    for folder in folders_to_link:
        source_path = os.path.join(source_dir, folder)
        target_path = os.path.join(target_dir, folder)

        if os.path.islink(target_path) or os.path.isfile(target_path):
            print(f"Folder {folder} already exists in {target_dir}")
        else:
            os.symlink(source_path, target_path)
            print(f"Linked folder {folder} to {target_dir}")

    for file in files_to_link:
        source_path = os.path.join(source_dir, file)
        target_path = os.path.join(target_dir, file)

        if os.path.islink(target_path): 
            print(f"File {file} already exists in {target_dir}")
        else:
            os.symlink(source_path, target_path)
            print(f"Linked file {file} to {target_dir}")

def link_zshrc():
    source_dir = os.path.expanduser("~/.dotfiles/.config/zsh")
    target_dir = os.path.expanduser("~/")

    files_to_link = ["zshrc", "p10k.zsh"]

    for file_to_link in files_to_link:
        source_path = os.path.join(source_dir, file_to_link)
        target_path = os.path.join(target_dir, "." + file_to_link)

        if os.path.islink(target_path) or os.path.isfile(target_path):
            print(f"File .{file_to_link} already exists in {target_dir}")
            if input("Do you want to overwrite the existing file? (y/n)").lower() == "y":
                os.remove(target_path)
                os.symlink(source_path, target_path)
                print(f".{file_to_link} has been overwritten with the linked file.")
            else:
                print(f".{file_to_link} is not overwritten.")
        else:
            os.symlink(source_path, target_path)
            print(f"Linked file {file_to_link} to .{file_to_link} in {target_dir}")

    zsh_path = shutil.which("zsh")
    if zsh_path:
        if os.environ.get("SHELL") != zsh_path:
            print("Changing default shell to zsh...")
            subprocess.run(["chsh", "-s", zsh_path])
    else:
        print("zsh not found. Please install zsh first.")


    # os.system(". ~/.zshrc")

def install_nvim_plugins():
    if check_if_app_exists("nvim"):
        if input("Do you want to install nvim plugins? (y/n): ").lower() == "y":
            print("Installing nvim plugins with Packer...")
            run_command('nvim --headless -c "autocmd User PackerComplete quitall" -c "PackerSync"')
        else:
            print("Nvim plugin installation skipped.")
    else:
        print("Nvim not found. Please install nvim first.")

def get_os_type():
    os_type = platform.system()
    if os_type == "Darwin":
        return "macos"
    elif os_type == "Linux":
        distro = platform.freedesktop_os_release()['NAME'].lower()
        if "ubuntu" in distro or "debian" in distro:
            return "debian_ubuntu"
    return "unsupported"

def source_tmux_config():
    if check_if_app_exists("tmux"):
        if os.path.isfile(os.path.expanduser("~/.tmux.conf")):
            if input("Do you want to source the tmux config? (y/n): ").lower() == "y":
                print("Sourcing tmux config...")
                run_command("tmux source ~/.tmux.conf")
            else:
                print("Sourcing tmux config skipped.")
        else:
            print("No tmux config found at ~/.tmux.conf. Skipping sourcing tmux config.")
    else:
        print("Tmux not found.")
        if input("Do you want to install tmux? (y/n): ").lower() == "y":
            os_type = platform.system()
            if os_type == "Darwin":
                print("Installing tmux with Homebrew...")
                run_command("brew install tmux")
            elif os_type == "Linux":
                distro = platform.freedesktop_os_release()['NAME'].lower()
                if "ubuntu" in distro or "debian" in distro:
                    print("Installing tmux with apt...")
                    run_command("sudo apt update && sudo apt install tmux")
                else:
                    print("Unsupported Linux distribution. Please install tmux manually.")
            else:
                print("Unsupported platform. Please install tmux manually.")
        else:
            print("Tmux installation skipped.")

def install_font():
    fonts_urls = [
        "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.1/JetBrainsMono.zip",
    ]  # Replace with the actual URLs of the font zip files

    if input("Do you want to install the necessary font(s)? (y/n): ").lower() == "y":
        for font_url in fonts_urls:
            file_name = os.path.basename(font_url)
            temp_file_path = os.path.join("/tmp", file_name)

            print(f"Downloading {font_url}...")
            urlretrieve(font_url, temp_file_path)

            with zipfile.ZipFile(temp_file_path, 'r') as zip_ref:
                print(f"Extracting {file_name}...")
                temp_extracted_folder = os.path.join("/tmp", os.path.splitext(file_name)[0])
                zip_ref.extractall(temp_extracted_folder)

            if platform.system() == "Darwin":
                fonts_dir = os.path.expanduser("~/Library/Fonts")
            elif platform.system() == "Linux":
                fonts_dir = os.path.expanduser("~/.local/share/fonts")
            else:
                print("Unsupported platform. Please install the font(s) manually.")
                return

            os.makedirs(fonts_dir, exist_ok=True)

            for item in os.listdir(temp_extracted_folder):
                if item.endswith(".ttf"):
                    source_path = os.path.join(temp_extracted_folder, item)
                    target_path = os.path.join(fonts_dir, item)

                    print(f"Copying {item} to {fonts_dir}...")
                    shutil.copy(source_path, target_path)
        print("Font(s) installation completed.")
    else:
        print("Font installation skipped.")

def install_zsh_plugins():
    zsh_custom = os.path.expanduser(os.getenv("ZSH_CUSTOM", "~/.oh-my-zsh/custom"))

    if not os.path.exists(zsh_custom):
        print(f"ZSH_CUSTOM directory not found: {zsh_custom}. Please ensure Oh My Zsh is installed.")
        return

    plugins_dir = os.path.join(zsh_custom, "plugins/")
    os.makedirs(plugins_dir, exist_ok=True)

    plugin_lists = [
        {
            "name": "zsh-autosuggestions", 
            "url": "https://github.com/zsh-users/zsh-autosuggestions.git",
            "path_to_clone": plugins_dir + "zsh-autosuggestions"
        },
        {
            "name": "zsh-syntax-highlighting",
            "url": "https://github.com/zsh-users/zsh-syntax-highlighting.git",
            "path_to_clone": plugins_dir + "zsh-syntax-highlighting"
        },
        {
            "name": "powerlevel10k",
            "url": "https://github.com/romkatv/powerlevel10k.git",
            "path_to_clone": zsh_custom + "/themes/powerlevel10k"
        }
    ]

    for plugin in plugin_lists:
        plugin_name = plugin["name"]
        plugin_url = plugin["url"]
        path_to_clone = plugin["path_to_clone"]

        if not os.path.exists(path_to_clone):
            print(f"Cloning {plugin_url} into {path_to_clone}...")
            run_command(f"git clone {plugin_url} {path_to_clone}")
        else:
            print(f"{path_to_clone} already exists. Skipping.")

def install_brew_and_packages():
    if platform.system() != "Darwin":
        print("Not a Mac. Skipping Homebrew and packages installation.")
        return

    if not check_if_app_exists("brew"):
        if input("Do you want to install Homebrew? (y/n): ").lower() == "y":
            print("Installing Homebrew...")
            run_command('/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"')
        else:
            print("Homebrew installation skipped.")
    else:
        print("Homebrew is already installed.")
        run_command("brew tap homebrew/cask")

    if os.path.exists("Brewfile"):
        if input("Do you want to install packages from Brewfile? (y/n): ").lower() == "y":
            print("Installing packages from Brewfile...")
            run_command("brew bundle")
        else:
            print("Package installation from Brewfile skipped.")
    else:
        print("Brewfile not found. Skipping package installation.")
# Example: install_font(), install_brew(), install_asdf(), etc.

def install_asdf():
    if os.path.exists(os.path.expanduser("~/.asdf")):
        print("asdf is already installed.")
    else:
        print("asdf not found.")
        if input("Do you want to install asdf? (y/n): ").lower() == "y":
            if not check_if_app_exists("git"):
                if input("Git is not installed. Do you want to install Git? (y/n): ").lower() == "y":
                    os_type = platform.system()
                    if os_type == "Darwin":
                        print("Installing Git with Homebrew...")
                        run_command("brew install git")
                    elif os_type == "Linux":
                        distro = platform.freedesktop_os_release()['NAME'].lower()
                        if "ubuntu" in distro or "debian" in distro:
                            print("Installing Git with apt...")
                            run_command("sudo apt update && sudo apt install git")
                        else:
                            print("Unsupported Linux distribution. Please install Git manually.")
                    else:
                        print("Unsupported platform. Please install Git manually.")
                else:
                    print("Git installation skipped.")
            
            print("Installing asdf...")
            run_command('git clone https://github.com/asdf-vm/asdf.git ~/.asdf')
        else:
            print("asdf installation skipped.")

def install_latest_apps():
    apps = ["nodejs", "awcli", "golang"]
    for app in apps:
        if check_if_app_exists("asdf"):
            if input(f"Do you want to update {app}? (y/n): ").lower() == "y":
                run_command(f"asdf install {app} latest && asdf global {app} $(asdf latest {app})")
                print(f"{app} updated successfully!")
            else:
                print(f"{app} update skipped.")
        else:
            print(f"asdf not found. Please install asdf first to update {app}.")

def main():
    source_dir = os.path.expanduser("~/.dotfile/.config")
    target_dir = os.path.expanduser("~/.config")

    create_config_directory()
    # install_brew_and_packages()
    install_oh_my_zsh()
    link_dotfiles()
    link_zshrc()
    install_font()
    install_asdf()
    install_latest_apps()
    install_nvim_plugins()
    source_tmux_config()
    install_zsh_plugins()


if __name__ == "__main__":
    main()
